<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
require '../core/ini.php';
//Check if person is logged
$isHelogged = new user();
if (!$isHelogged->isLoggedIn()) {
  echo helper::outcome(3, false);
  return;
}
//Is the checkbox checked
if (Input::defined('cancel') && Input::get('cancel') === 'on') {
  //Check if the person is acommunity admin
  $community = new community(Session::get('communityAdmin'));
  if ($community->isAdmin(Session::get('user'))) {
    //Prepare the code to send
    //Log code on the community field and deactivate account
    if ($community->deactivate()) {
      echo helper::outcome(462, true);
      return;
    }
  } else {
    echo helper::outcome(400, false);
  }
} else {
  echo helper::outcome(458, false);
  return;
}
