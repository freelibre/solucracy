<?php
// copyright (c) 2018 - Yannick Laignel <contact@solucracy.com>
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see https://www.gnu.org/licenses/
require '../core/ini.php';
if (Input::defined('term')) {
  $data          = Input::get('term');
  $communityList = helper::getCommunityList($data, 1);
  $jsonfile      = json_encode($communityList);
  echo $jsonfile;
} elseif (Input::defined('communityId')) {
  $community = new community(Input::get('communityId'));
  if (Input::get('value') > 0) {
    if ($community->subscribe()) {
      echo helper::outcome(500, true); //You just subscribed to this community
      exit();
    } else {
      echo helper::outcome(11, false); //There's been a problem
      exit();
    }
  } else {
    if ($community->unsubscribe()) {
      echo helper::outcome(501, true); // you unsubscribed from this community
      exit();
    } else {
      echo helper::outcome(11, false); //There's been a problem
      exit();
    }
  }

}
