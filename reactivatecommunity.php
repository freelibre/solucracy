<?php
require 'core/ini.php';
helper::loadHeader('header.php', array(
  'TITLE' => $_SESSION['words'][463]
  , 'DESCRIPTION' => $_SESSION['words'][464]));
//if the code is in the URL
if (Input::defined('code')) {
  $community = new community();
  //and it matches a community, and it's not more than 30 days old
  if ($community->checkCode(Input::get('code'))) {
    //redirect to a page to enter payment info
    echo '<p>Si Yannick avait fait son boulot, vous seriez redirigé(e) vers les infos de paiement...</p>';
  } else {
    //otherwise let the user know
    echo '<p>' . $_SESSION['words'][481] . '</p>';
    return;
  }
} else {
  echo '<p>Oui ? vous cherchez quelque chose ?</p>';
}
